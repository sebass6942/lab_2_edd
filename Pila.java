
/**
 * Clase que define las operaciones básicas que debe tener una pila.
 * @author xavier
 */
public class Pila {
    // Puntero que indica el inicio de la pila
    private NodoPila inicio;
    // tope de la pila.
    private NodoPila termino;
    // Variable para registrar el tamaño de la pila.
    private int tamanio;
    /**
     * Constructor por defecto.
     */
    public void Pila(){
        inicio = null;
        termino = null;
    }
    /**
     * Consulta si la pila esta vacia.
     * @return true si el primer nodo (inicio), no apunta a otro nodo.
     */
    public boolean esVacia(){
        return inicio == null;
    }
    
     //Metodo insertar
    
    public void insertar(String nombre ,int edad,String rut) {
        //crear e instertar nodo pila
    	NodoPila nuevo= new NodoPila(nombre,edad,rut,null);
        //metodo instertar en pila
        if (inicio==null)// si es null entra, el siguiente en null y inicio es nuevo
        {
            nuevo.siguiente = null;
            inicio = nuevo;
        }
        else//si no pasa a inicio y inicio es nuevo
        {
            nuevo.siguiente = inicio;
            inicio = nuevo;
        }
        
        
    }
   //metodo imprimir pila
    public void imprimir() {
        // nuevo nodo inicio
        NodoPila reco = inicio;
        System.out.println("Listado de todos los elementos de la pila.");
        while (reco!=null) { //mientras sea null entra
            System.out.print("nombre: "+reco.getNombre()+" edad: "+reco.getEdad()+" rut: "+reco.getRut()+" ");
            reco=reco.siguiente;//el nodo pasa a siguiente
        }
        System.out.println();
    }
   
}