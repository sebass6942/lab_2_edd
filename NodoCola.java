
/**
 *
 * @author sebastian
 */
public class NodoCola {
    NodoCola next;
    //variable para guardar nombre.
    private String nombre;
    //variable para guardar edad.
    private int edad;
    // Variable en la cual se va a guardar el valor rut.
    private String rut;
    // Variable para enlazar los nodos.
  
 //Constructor
 public NodoCola(String nombre, int edad, String rut,NodoCola next){
    this.nombre = nombre;
    this.edad = edad;
    this.rut = rut;
    this.next=null;
 }
 //Metodos getter and setters

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }
 
    public NodoCola getNext() {
        return next;
    }
 
    public void setNext(NodoCola next) {
        this.next = next;
    }
   
}