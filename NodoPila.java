/**
 * Clase que define los elementos que debe tener un Nodo de la lista.
 * @author xavier
 */
public class NodoPila {
    NodoPila siguiente;
    //variable para guardar nombre.
    private String nombre;
    //variable para guardar edad.
    private int edad;
    // Variable en la cual se va a guardar el valor rut.
    private String rut;
    // Variable para enlazar los nodos.
    //Constructor
    public NodoPila(String nombre, int edad, String rut,NodoCola siguiente) {
        this.nombre = nombre;
        this.edad = edad;
        this.rut = rut;
        this.siguiente=null;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public NodoPila getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoPila siguiente) {
        this.siguiente = siguiente;
    }
    
}
