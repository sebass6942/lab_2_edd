
/**
 *
 * @author sebastian
 */
public class Colas {
    //Declaración de atributos
    private NodoCola inicio;
    private NodoCola termino;
 
 //Constructor sin parametros
 public Colas()
 {
    inicio=null;
    termino=null;
 }
  
 //Metodo insertar
 public void insertarC(String nombre ,int edad,String rut) { 
     //**INTENTO**//
     //verificamos si el rut es valido
     //if (validarRut(rut)==true) {
     //nuevo nodo con sus datos 
     NodoCola i=new NodoCola(nombre,edad,rut,null);
     i.setNext(null);
     //ingresar en cola
        if (inicio == null) {
            inicio = i;
        } else {
            termino.next = i;
        }
        termino =i;
     //}
     //else{
       //  System.out.println("EL rut no es valido");
     //}
 }
 
 //Metodo para comprobar que la cola no esta vacia
 public boolean estaVacia(){
    boolean cola=false;
    if(inicio==null & termino==null){
        cola=true;
        System.out.println("La cola esta vacia");
    }
    else{
        System.out.println("La cola no esta vacia");
        cola=false;
    }
    return cola;
 }
 
  //metodo imprimir cola
    public void imprimir() {
        NodoCola reco = inicio;
        System.out.println("Listado de todos los elementos de la cola.");
        //mientras el nodo sea distinto de nulo sigue
        while (reco!=null) {
            System.out.print("nombre: "+reco.getNombre()+" edad: "+reco.getEdad()+" rut: "+reco.getRut()+" ");// imprime
            reco=reco.next; // pasa al nodo siguiente
        }
        System.out.println();
    }
    
    //INTENTO DE metodo para validar el rut que despues de 2 se cae
    public static boolean validarRut(String rut) {
 
        boolean validacion = false;
        try {
            rut = rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));
 
            char dv = rut.charAt(rut.length() - 1);
 
            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) { //para rutAux = valor del rut mientras sea distinto de 0 recorrer 
                                                //y por cada iteración dividir en rutAux en 10
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) { //0 suma al resultado 47 para representar un numero del 0 al 9 como dígito verificador
                validacion = true;
            }
 
        } 
        catch (java.lang.NumberFormatException e) {
        } 
        catch (Exception e) {
        }
        return validacion;
    }
}
