
import java.io.*;

/**
 * Clase principa que implementa los metodos de la clase pila.
 * @author xavier
 */
public class Main {
    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String x[]) throws IOException {
        BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in)); 
        Colas cola = new Colas();
        Pila pila=new Pila();
        String nombre,rut;
        int edad;
        System.out.println("<<-- Ejemplo de Pila -->>\n\n");
        
        cola.insertarC("sebastian",15,"2.312.313-4");
        pila.insertar("sebastian",15,"2.312.313-4");
        cola.insertarC("feña",20,"4.312.813-k");
        pila.insertar("feña",20,"4.312.813-k");
        cola.insertarC("nicolas",15,"2.312.313-4");
        pila.insertar("nicolas",23,"6.714.613-3");
        cola.insertarC("margo",18,"8.212.344-1");
        pila.insertar("margo",18,"8.212.344-1");
        int opc; 
            
            do
            {
                System.out.print("\n  --------MENU-------- ");  
                System.out.print("\n  | 1.Insertar  Nodo ");   
                System.out.print("\n  | 2.Mostrar  Lista "); 
                System.out.print("\n  | 3.Salir ");
                System.out.print("\n Selecciona una opcion: "); 

                opc=Integer.parseInt(entrada.readLine());
                                      
                switch(opc)  
                 {
                  case 1: System.out.print("\n Ingrese nombre: ");
                          nombre =entrada.readLine(); 
                          System.out.print("\n Ingrese edad: ");
                          edad=Integer.parseInt(entrada.readLine());
                          System.out.print("\n Ingrese rut: ");
                          rut=entrada.readLine();
                          cola.insertarC(nombre,edad,rut);
                          pila.insertar(nombre,edad,rut);
                      break;
                    
                  case 2: cola.imprimir();
                          pila.imprimir();
                      break;
                      
                default:    System.out.print("\nTERMINADO");
                            
            }
      }
            while(opc<=2);     
    }
}